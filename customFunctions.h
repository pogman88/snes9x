/*
 * customFunctions.h
 *
 *  Created on: 15 de jul de 2016
 *      Author: fernando
 */

#ifndef CUSTOMFUNCTIONS_H_
#define CUSTOMFUNCTIONS_H_

#include "snes9x.h"
#include "memmap.h"
#include "cheats.h"

// Rewards for DRL
#define LIFEBAR_PLAYER1_1 0x7e0d12
#define LIFEBAR_PLAYER1_2 0x7e0c35
#define LIFEBAR_PLAYER1_3 0x7e0c2b

#define LIFEBAR_PLAYER2_1 0x7e0e2b
#define LIFEBAR_PLAYER2_2 0x7e0e35
#define LIFEBAR_PLAYER2_3 0x7e0f12

#define MATCHTIME 0x7e1ac8

uint8 GetLifeBarP1();
uint8 GetLifeBarP2();
uint8 GetMatchTime();



#endif /* CUSTOMFUNCTIONS_H_ */
