/*
 * customFunctions.cpp
 *
 *  Created on: 15 de jul de 2016
 *      Author: fernando
 */


#include "customFunctions.h"

#define LIFEBAR_PLAYER1 0x7E0D12

static uint8 CFGetByteFree (uint32);

static uint8 CFGetByteFree (uint32 address)
{
	uint32	Cycles = CPU.Cycles;
	uint8	byte;

	byte = S9xGetByte(address);
	CPU.Cycles = Cycles;

	return (byte);
}

uint8 GetLifeBarP1() {
	uint32 lf = LIFEBAR_PLAYER1_1;
	uint8 r = CFGetByteFree(lf);
	return r;
}

uint8 GetLifeBarP2() {
	uint32 lf = LIFEBAR_PLAYER2_1;
	uint8 r = CFGetByteFree(lf);
	return r;
}

uint8 GetMatchTime() {
	uint32 lf = MATCHTIME;
	uint8 r = CFGetByteFree(lf);
	return r;
}



